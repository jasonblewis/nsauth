//import { SECRET_TOKEN } from '$env/static/private'

export const load = async () => {
  const fetchCoins = async () => {
    const req = await fetch('https://api.coinlore.com/api/tickers/')
    const { data } = await req.json()
    return data
  }

  return {
    currencies: fetchCoins(),
  }
}