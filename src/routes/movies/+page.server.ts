import type { PageServerLoad } from './$types';
import {TMDB_API_KEY} from '$env/static/private';

export async function load({ fetch }) {

        const [movies] = await Promise.all([
            fetch(`https://api.themoviedb.org/3/trending/movie/week?api_key=${TMDB_API_KEY}`).then(r => r.json()),
        ]);
        console.log(movies.results);
        let movielist = movies.results;
        return { movielist };
    }