import { PUBLIC_PETFINDER_API_KEY } from '$env/static/public';
import { SECRET_PETFINDER_API_SECRET } from '$env/static/private';
export function load () {
    console.log(PUBLIC_PETFINDER_API_KEY);
    console.log(SECRET_PETFINDER_API_SECRET);

    // Call the API
    // This is a POST request, because we need the API to generate a new token for us
    fetch('https://api.petfinder.com/v2/oauth2/token', {
	method: 'POST',
	body: 'grant_type=client_credentials&client_id=' + PUBLIC_PETFINDER_API_KEY + '&client_secret=' + SECRET_PETFINDER_API_SECRET,
	headers: {
		'Content-Type': 'application/x-www-form-urlencoded'
	}
}).then(function (resp) {

	// Return the response as JSON
	return resp.json();

}).then(function (data) {

	// Log the API data
	console.log('token', data);

}).catch(function (err) {

	// Log any errors
	console.log('something went wrong', err);

})};
